define(['controllers/AppCtrl'], function(AppCtrl) {
  'use strict';

  describe('AppCtrl as JavaScript contructor', function() {

    var $scope = {};
    it('should $scope.company to Eyeota', function() {
      // given
      var expected = 'Eyeota';

      // when
      new AppCtrl($scope);

      //then
      expect($scope.company).toEqual(expected);
    });

  });
});