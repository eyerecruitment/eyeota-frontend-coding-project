define(['angular', 'controllers/AppCtrl', 'angular-route'], function(angular, AppCtrl) {
  'use strict';

  var app = angular.module('mainApp', ['ngRoute']);

  app.controller('AppCtrl', AppCtrl);

  return app;
});
