# Eyeota FrontEnd coding project

## The challenge

The objective is to build a reusable *analog clock* component similar to the following one:

![clock.png](https://bitbucket.org/repo/5MzL47/images/2897370627-clock.png)

using one of the modern frameworks: AngularJS, Ember.js, ReactJS or similar - **do let us know beforehand**

### Requirements

* the clock should be an component (or an equivalent of a component in some frameworks)
* clock component should be customizable with the following options
    * `background-color` defines the color of the clock face
    * `seconds-tick-enabled` (*true|false*) specifies whether the seconds tick is visible
    * `offset` given in hours - e.g. `offset="2"` means the current time + 2 hours, `offset="-3.5"` means current time - 3.5 hours
* possibility to have more than one clock component on the page (with different options passed)
* you should use SVG to achieve the desired result (you may use a library like [D3.js](http://d3js.org/), [Rapha�l](http://raphaeljs.com/) or [Snap.svg](http://snapsvg.io/) or even handwritten SVG)
* the code should be tested and *jslint* errors and warnings free
* you can add own dependencies
* you can use build tools that you are familiar with (Gulp, Grunt, Webpack, etc)

## Project structure

You are free to structure the project as you wish. What we are looking for is your ability to architect the code that 
is clean, easy to understand and testable

# Result

You should send the result in a *.zip* file or *tarball* (created with command `tar -czvf`) file and **should not** push it to the public repo.

# License

Copying, sharing or publishig is not allowed.

Copyright 2014-2016 Eyeota